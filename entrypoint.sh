#!/bin/sh
set -e

cd /e2e

ping dev.dp3.integration.dev.doxee.com -c 4
ping login.dp3.integration.dev.doxee.com -c 4

curl https://dev.dp3.integration.dev.doxee.com

npm install typescript
export EMAIL=${EMAIL}
export PASSWORD=${PASSWORD}
echo "Email userd $EMAIL"
exec cypress run "$@"